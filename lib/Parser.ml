(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Base = struct

  type 'a res =
    | Val of 'a * char list
    | Err
  type 'a t = char list -> 'a  res

  let return a = fun l -> Val (a,l)

  let bind m f = fun l -> match m l with
                          | Val (x,q) -> f x q
                          | Err -> Err


end

module M = Monad.Expand (Base)
include M
open Base

let fail () = fun l -> Err

let any () = fun l -> match l with
                      | [] -> Err
                      | c :: q -> Val (c,q)

let empty () = fun l -> match l with
                        | [] -> Val ((),[])
                        | _ -> Err

let symbol c = fun l -> match l with
                        | t :: q when t = c -> Val ((),q)
                        | _ -> Err

let either m1 m2 = fun l -> match m1 l with
                            | Err -> m2 l
                            | v -> v

let optionally m = fun l -> match m l with
                            | Err -> Val (None,l)
                            | Val (x,q) -> Val (Some x,q)

let rec star m = fun l -> match plus m l with
                          | Err -> Val ([],l)
                          | v -> v
and plus m = fun l -> match m l with
                      | Err -> Err
                      | Val (x,q) -> match star m q with
                                     | Val (vals, q') -> Val (x::vals,q')
                                     | _ -> failwith "Impossible"

exception ParsingError

let run m toks = match m toks with
  | Val (v,[]) -> v
  | Val (v,l) -> print_string "Warning : didn't parse everything\n" ; v
  | Err -> raise ParsingError


(* TODO: add a backtracking operator? *)
