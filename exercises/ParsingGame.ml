(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-33-37-39"]

open Monads

(* Taken from [https://wiki.haskell.org/State_Monad#Complete_and_Concrete_Example_1] *)

(* Passes a string of dictionary {a,b,c}
 * Game is to produce a number from the string.
 * By default the game is off, a 'c' toggles the
 * game on and off.
 * A 'a' gives +1 and a 'b' gives -1 when the game is on,
 * nothing otherwise.
 *)

module S = State.Make(struct type t = (bool*int*int) let init = (false,0,0) end)
open S

let playGame s =
  let rec aux () =
    let* (on, pos, value) = get () in
    if pos = String.length s then return value
    else let* _ = set ((if s.[pos] = 'c' then not on else on),
                       pos + 1,
                       match on, s.[pos] with
                       | true, 'a' -> value + 1
                       | true, 'b' -> value - 1
                       | _, _ -> value) in
         aux () in aux ()
    
      


let result s = run (playGame s)

let%test _ = result "ab" = 0
let%test _ = result "ca" = 1
let%test _ = result "cabca" = 0
let%test _ = result "abcaaacbbcabbab" = 2
